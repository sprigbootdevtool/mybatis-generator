# Mybatis-Generator
SQL反向自動生成程式碼

step1<br/>

mybatis-generator plugin <br/>
版本套件需使用的部份 (Gradle版本)
```
dependencies {
        classpath 'gradle.plugin.com.thinkimi.gradle:mybatis-generator-plugin:2.3'
    }
}

plugins {
    id 'com.thinkimi.gradle.MybatisGenerator' version '2.3'
}
apply plugin: 'com.thinkimi.gradle.MybatisGenerator'

dependencies {
    testImplementation 'org.mybatis.generator:mybatis-generator-core:1.3.2'
}

//可客製化修改
mybatisGenerator {
    verbose =true
    configFile ='src/main/resources/generatorConfig.xml'
}
```

step2<br/>
生成 generatorConfig.xml
```
project
|───src
|   |───main
|   |   |───java
|   |   |───resources
|   |       |─── generatorConfig.xml<--在此生成
|   |───test
│───README.md    
```
generatorConfig.xml內容<br/>
參考mybatis-generator手冊-https://mybatis.org/generator/configreference/xmlconfig.html