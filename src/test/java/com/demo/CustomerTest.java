package com.demo;

import com.demo.dao.CustomerMapper;
import com.demo.models.Customer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.logging.Logger;


@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class CustomerTest {
    private static Logger logger =Logger.getLogger(CustomerTest.class.getName());


    @Autowired
    private CustomerMapper customerMapper;

    @Test
    public void testQuery() throws Exception {
        try{

            Customer customerinfo= customerMapper.selectByPrimaryKey(3);

            System.out.println(customerinfo.toString());
        }catch (Exception e){
            logger.warning(e.getMessage());
        }
    }

    @Test
    public void testInsert() throws Exception{
        try{
            Customer customerInfo=new Customer();
            customerInfo.setId(3);
            customerInfo.setName("Benson03");
            customerInfo.setPhone("0911000999");
            customerMapper.insert(customerInfo);
            logger.info("Insert");
        }catch (Exception e){
            logger.warning(e.getMessage());
        }
    }
}
