CREATE DATABASE IF NOT EXISTS `playerstage`;
USE `playerstage`;

CREATE TABLE `products_crawlered_record` (
  `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `utime` DATETIME DEFAULT NOW() ON UPDATE NOW(),
  `product_id` BINARY(16) NOT NULL,
  `original_shopee_itemid` VARCHAR(45) NOT NULL,
  `has_been_edited` BOOLEAN NOT NULL DEFAULT false
);

CREATE TABLE IF NOT EXISTS `products` (
  `uuid` BINARY(16) NOT NULL PRIMARY KEY,
  `name` VARCHAR(100) NOT NULL,
  `ctime` DATETIME NOT NULL DEFAULT NOW(),
  `utime` DATETIME DEFAULT NOW() ON UPDATE NOW(),
  `brand` VARCHAR(80) NOT NULL,
  `is_pre_order` BOOLEAN NOT NULL DEFAULT false,
  `rate` FLOAT NOT NULL DEFAULT 0,
  `description` VARCHAR(1000) NOT NULL,

  `model_name` VARCHAR(30) NOT NULL
) COMMENT = '賣場商品';